package com.fusetech.enumstatemachine.function_strategy;

/**
 * @Author：Liuwq-Fuse
 * @Package：function_strategy
 * @Project：fuse-tech
 * @name：RoleContant
 * @Date：2023/6/25 9:58
 * @Filename：RoleContant
 */
public interface RoleContant {

    public static final String BA = "BA";

    public static final String TEACH = "TEACH";

}
