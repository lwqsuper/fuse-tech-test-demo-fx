package com.fusetech.enumstatemachine.function_strategy.consumer;



//import javax.validation.constraints.NotNull;
import com.sun.istack.internal.NotNull;

import java.util.function.Consumer;

/**
 * @Author：Liuwq-Fuse
 * @Package：function_strategy.consumer
 * @Project：fuse-tech
 * @name：Throwing
 * @Date：2023/6/27 15:56
 * @Filename：Throwing
 */
public class Throwing {
    private Throwing() {}

    @NotNull
    public static <T> Consumer<T> rethrow(@NotNull final ThrowingConsumer<T> consumer) {
        return consumer;
    }

    /**
     * The compiler sees the signature with the throws T inferred to a RuntimeException type, so it allows the unchecked
     * exception to propagate.
     *
     * http://www.baeldung.com/java-sneaky-throws
     */
    @SuppressWarnings("unchecked")
    @NotNull
    public static <E extends Throwable> void sneakyThrow(@NotNull final Throwable ex) throws E {
        throw (E)ex;
    }
}
