package com.fusetech.enumstatemachine.function_strategy.consumer;


//import org.junit.rules.ExpectedException;
//import org.testng.annotations.Test;
//
//import java.io.IOException;
//import java.util.Arrays;
//
//import static com.taozhugongboy.enumstatemachine.function_strategy.consumer.Throwing.rethrow;


/**
 * @Author：Liuwq-Fuse
 * @Package：function_strategy.consumer
 * @Project：fuse-tech
 * @name：ThrowingTest
 * @Date：2023/6/27 15:58
 * @Filename：ThrowingTest
 */
public class ThrowingTest {
    /*@Rule
    public ExpectedException thrown = ExpectedException.none();*/
    /*public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testRethrow() {
        thrown.expect(IOException.class);
        thrown.expectMessage("i=3");

        Arrays.asList(1, 2, 3).forEach(rethrow(e -> {
            int i = e;
            if (i == 3) {
                throw new IOException("i=" + i);
            }
        }));
    }

    //@Test(expected = IOException.class)
    public void testSneakyThrow() {
        Throwing.sneakyThrow(new IOException());
    }

    @Test
    public void testThrowingConsumer() {
        thrown.expect(IOException.class);
        thrown.expectMessage("i=3");

        Arrays.asList(1, 2, 3).forEach((ThrowingConsumer<Integer>)e -> {
            int i = e;
            if (i == 3) {
                throw new IOException("i=" + i);
            }
        });
    }

    public void testMapThrowingConsumer() {

    }
*/
}
