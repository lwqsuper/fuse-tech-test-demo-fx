package com.fusetech.enumstatemachine.function_strategy;

/**
 * @Author：Liuwq-Fuse
 * @Package：function_strategy
 * @Project：fuse-tech
 * @name：TestConstant
 * @Date：2023/6/25 10:21
 * @Filename：TestConstant
 */
public enum TestConstant {
    /**
     * 无效
     **/
    Invalid ( "0", "无效" ),
    /**
     * 待审核
     **/
    Under_Audit ( "1", "待审核" ),
    /**
     * 审核中
     **/
    Auditing ( "2", "审核中" ),
    /**
     * 审核通过
     **/
    Audit_Pass ( "3", "审核通过" ),
    /**
     * 驳回
     **/
    Reject ( "4", "驳回" ),
    /**
     * 撤销
     **/
    Cancel ( "5", "撤销" ),

    /**
     * 草稿
     */
    Draft ( "6", "草稿" );

    /**
     * 类型值
     */
    private String value;

    /**
     * 名称
     */
    private String name;

    private TestConstant(String value, String name) {
        this.value = value;
        this.name = name;
    }

    /**
     * 数据值
     *
     * @return {@link String }
     * @author Liuwq
     * @date 2023/06/15
     */
    public String getValue() {
        return value;
    }

    /**
     * 中文名
     *
     * @return {@link String }
     * @author Liuwq
     * @date 2023/06/15
     */
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.value+":"+this.name;
    }
}
