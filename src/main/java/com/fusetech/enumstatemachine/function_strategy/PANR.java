package com.fusetech.enumstatemachine.function_strategy;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Java 小技巧-Function+Map实现策略模式
 *
 * @Author：Liuwq-Fuse
 * @Package：function_strategy
 * @Project：fuse-tech
 * @name：PANR
 * @Date：2023/6/25 9:44
 * @Filename：PANR
 */
public class PANR {
    static Map<TestConstant, Consumer<String>> map = new HashMap();
    static {
        map.put(TestConstant.Invalid,s -> BA(s));
    }

    public static void BA(String name){
        System.out.println("BA谁都包补了"+name);
    }

    public static void handle(TestConstant testConstant,String value){
        map.get(testConstant).accept(value);
    }

    public static void main(String[] args) {
        //handle(TestConstant.Invalid,"王麻子");

        handle(TestConstant.valueOf("1"),"王麻子");

    }
}
