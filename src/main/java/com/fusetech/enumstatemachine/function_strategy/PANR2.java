package com.fusetech.enumstatemachine.function_strategy;



import com.fusetech.enumstatemachine.function_strategy.consumer.ServiceException;
import com.fusetech.enumstatemachine.function_strategy.consumer.ThrowingConsumer;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Java 小技巧-Function+Map实现策略模式
 *
 * @Author：Liuwq-Fuse
 * @Package：function_strategy
 * @Project：fuse-tech
 * @name：PANR2
 * @Date：2023/6/25 10:44
 * @Filename：PANR2
 */
public class PANR2 {
    static Map<String, Consumer<String>> map = new HashMap();

    static Map<String, ThrowingConsumer<String>> ppmap = new HashMap();
    static {
        //map.put(RoleContant.BA,s -> BA(s));

        map.put(RoleContant.BA,s -> {
            try {
                JJ(s);
            } catch (ServiceException e) {
                e.printStackTrace();
            }
        });

        ppmap.put(RoleContant.BA,s -> JJ(s));

        /*ppmap.put(RoleContant.BA,s -> {
            try {
                JJ(s);
            } catch (ServiceException e) {
                e.printStackTrace();
            }
        });*/
    }

    public static void BA(String name){
        System.out.println("BA谁都包补了"+name);
    }

    public static void JJ(String name) throws ServiceException {
        throw new ServiceException("审核记录保存失败");
    }

    public static void handle(String testConstant,String value){
        map.get(testConstant).accept(value);
    }

    public static void handlepp(String testConstant,String value){
        ppmap.get(testConstant).accept(value);
    }

    public static void main(String[] args) {
        //handle(TestConstant.Invalid,"王麻子");

        handle("BA","王麻子");
        //handlepp("BA","王麻子");
    }
}
